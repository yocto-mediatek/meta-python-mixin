# meta-python-mixin

## Introduction

This layer is intended to be an extension to the meta-python layer. It
(meta-python-mixin) will provide additional modules, versions or various other
functionality backported from meta-python later branches. It does that by
maintaining equivalent LTS branches (e.g. dunfell) where this support is
maintained.

The aim of this is to minimise duplication on python recipes backporting on
other layers. For example, meta-homeassistant.

## Yocto Project Compatible Layer

<img src="assets/LF_17_02_Yocto-Badge-Update_Compatible_Final_Gold.png" alt="Yocto Compatible Gold Badge" width="100"/>

This layer is officially approved as part of the `Yocto Project Compatible
Layers Program`. You can find details of that on the official Yocto Project
[website](https://www.yoctoproject.org/software-overview/layers/?searchTerm=meta-python-mixin).

## Dependencies

The meta-python-mixin layer depends on:

* URI: git://git.openembedded.org/openembedded-core
  * layers: meta
  * branch: dunfell
  * revision: HEAD
* URI: git://git.openembedded.org/meta-openembedded
  * layers: meta-python
  * branch: dunfell
  * revision: HEAD

Please follow the recommended setup procedures of your OE distribution.

## Contributing

Contributions to this layer are to be handled as merge requests in the Gitlab
instance where the layer is hosted:
https://booting.oniroproject.org/distro/meta-python-mixin.

## Maintenance

Maintainers:

* Andrei Gherzan <andrei.gherzan@huawei.com>
* Eilís Ní Fhlannagáin <elizabeth.flanagan@huawei.com>
* Stefan Schmidt <stefan.schmidt@huawei.com>

## License

See the `LICENSES` subdirectory.
